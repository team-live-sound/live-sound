var path = require('path');
var webpack = require('webpack');

const bourbon = require("node-bourbon");
const neat = require("node-neat");
const vendorStylePath = "./vendor/assets/stylesheets";
const localStylePath = "./app/assets/stylesheets";

const stylePaths = bourbon.includePaths
  .concat(neat.includePaths)
  .concat(path.resolve(__dirname, localStylePath))
  .concat(path.resolve(__dirname, vendorStylePath))
  .map(function (sassPath) {
    return "includePaths[]=" + stylePaths;
  }).join("&");


const env = process.env.npm_config_env || "development";
const isDevServer = process.argv[1].indexOf("webpack-dev-server") !== -1;

module.exports = {
  entry: './app/scripts/main.jsx',
  output: { path: __dirname, filename: 'bundle.js' },
  devServer: {
    port: 5000,
    historyApiFallback: {
      index: 'index.html'
    }
  },
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.scss?$/,
        loaders: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg)$/,
          loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
      },
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      _: "lodash",
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    })
  ],
};
