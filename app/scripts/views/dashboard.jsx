import React from 'react';

export default class Dashboard extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3">
            <h1>Sidebar</h1>
          </div>
          <div className="col-md-9">
            <h1>Main</h1>
          </div>
        </div>
      </div>
    )
  }
}
